package com.appcent.appcenttodo.controller;

import com.appcent.appcenttodo.domain.User;
import com.appcent.appcenttodo.repository.ItemRepository;
import com.appcent.appcenttodo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ItemRepository itemRepository;

    @GetMapping("/users")
    public List<User> getUsers() {
        return (List<User>) userRepository.findAll();
    }

    @PostMapping("/users")
    void addUser(@RequestBody User user) {
        user.setCreatedBy("fromFrontEnd");
        userRepository.save(user);
    }



}