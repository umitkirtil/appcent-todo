package com.appcent.appcenttodo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;

@SpringBootApplication
public class AppcentTodoApplication {
	public static void main(String[] args) {
		SpringApplication.run(AppcentTodoApplication.class, args);
	}
}