package com.appcent.appcenttodo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A Item.
 */
@Entity
@Table(name = "item")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Item implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "aciklama")
    private String aciklama;

    @Column(name = "yapildi_mi")
    private Boolean yapildiMi;

    @ManyToOne
    @JsonIgnoreProperties(value = { "users" }, allowSetters = true)
    private ItemGroup itemGroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Item id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAciklama() {
        return this.aciklama;
    }

    public Item aciklama(String aciklama) {
        this.setAciklama(aciklama);
        return this;
    }

    public void setAciklama(String aciklama) {
        this.aciklama = aciklama;
    }

    public Boolean getYapildiMi() {
        return this.yapildiMi;
    }

    public Item yapildiMi(Boolean yapildiMi) {
        this.setYapildiMi(yapildiMi);
        return this;
    }

    public void setYapildiMi(Boolean yapildiMi) {
        this.yapildiMi = yapildiMi;
    }

    public ItemGroup getItemGroup() {
        return this.itemGroup;
    }

    public void setItemGroup(ItemGroup itemGroup) {
        this.itemGroup = itemGroup;
    }

    public Item itemGroup(ItemGroup itemGroup) {
        this.setItemGroup(itemGroup);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Item)) {
            return false;
        }
        return id != null && id.equals(((Item) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Item{" +
            "id=" + getId() +
            ", aciklama='" + getAciklama() + "'" +
            ", yapildiMi='" + getYapildiMi() + "'" +
            "}";
    }
}
