package com.appcent.appcenttodo.initialize;

import com.appcent.appcenttodo.domain.Authority;
import com.appcent.appcenttodo.domain.Item;
import com.appcent.appcenttodo.domain.ItemGroup;
import com.appcent.appcenttodo.domain.User;
import com.appcent.appcenttodo.repository.AuthorityRepository;
import com.appcent.appcenttodo.repository.ItemGroupRepository;
import com.appcent.appcenttodo.repository.ItemRepository;
import com.appcent.appcenttodo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DummyDataCreate {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthorityRepository authorityRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    ItemGroupRepository itemGroupRepository;

    @Autowired
    ItemRepository itemRepository;

    String prefix_imageurl = "http://localhost.8080/images/";

    // @Autowired annation'ın calismasi icin Constructor'dan sonra calisan bir mekanizma olan postconstruct ile dummy data olusturuyoruz.
    @PostConstruct
    public void createDummyH2Database() {

        // create authorities if does not exist.
        Authority adminAuth = authorityRepository.findByName("ADMIN").orElse(new Authority("ADMIN"));
        authorityRepository.save(adminAuth);

        Authority userAuth = authorityRepository.findByName("USER").orElse(new Authority("USER"));
        authorityRepository.save(userAuth);
        // create authorities if does not exist.

        // add one admin and few users.

        User admin = new User();
        admin.setActivated(true);
        admin.setLogin("admin");
        admin.setPassword(passwordEncoder.encode("admin"));
        admin.setEmail("admin@admin.com");
        admin.setFirstName("Admin");
        admin.setLastName("Admin");
        admin.setImageUrl(prefix_imageurl + "adminlogo2.png");
        admin.setCreatedBy("system");
        admin.getAuthorities().add(adminAuth);
        admin.getAuthorities().add(userAuth);

        userRepository.save(admin);

        /////////////////////////////////////////
        User user1 = new User();
        user1.setActivated(true);
        user1.setLogin("user1");
        user1.setPassword(passwordEncoder.encode("user1"));
        user1.setEmail("user1@admin.com");
        user1.setFirstName("User 1");
        user1.setLastName("User 1");
        user1.setImageUrl(prefix_imageurl + "user1.png");
        user1.setCreatedBy("system");
        user1.getAuthorities().add(userAuth);

        userRepository.save(user1);


        /////////////////////////////////////////

        /////////////////////////////////////////
        User user2 = new User();
        user2.setActivated(true);
        user2.setLogin("user2");
        user2.setPassword(passwordEncoder.encode("user2"));
        user2.setEmail("user2@admin.com");
        user2.setFirstName("User 2");
        user2.setLastName("User 2");
        user2.setImageUrl(prefix_imageurl + "user2.png");
        user2.setCreatedBy("system");
        user2.getAuthorities().add(userAuth);

        userRepository.save(user2);

        /////////////////////////////////////////

        /////////////////////////////////////////
        User user3 = new User();
        user3.setActivated(true);
        user3.setLogin("user3");
        user3.setPassword(passwordEncoder.encode("user3"));
        user3.setEmail("user3@admin.com");
        user3.setFirstName("User 3");
        user3.setLastName("User 3");
        user3.setImageUrl(prefix_imageurl + "user3.png");
        user3.setCreatedBy("system");
        user3.getAuthorities().add(userAuth);

        userRepository.save(user3);

        /////////////////////////////////////////
        // add one admin and few users.

        ItemGroup mutfakIsi = new ItemGroup();
        mutfakIsi.aciklama("Mutfak Isleri");
        mutfakIsi.getUsers().add(admin);

        itemGroupRepository.save(mutfakIsi);

        Item yemekYap = new Item();
        yemekYap.setAciklama(" Yemek Yap");
        yemekYap.setYapildiMi(false);

        yemekYap.setItemGroup(mutfakIsi);

        itemRepository.save(yemekYap);
        ///////////////////////////////////////////

    }

}
