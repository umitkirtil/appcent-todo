package com.appcent.appcenttodo.repository;

import com.appcent.appcenttodo.domain.ItemGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data SQL repository for the ItemGroup entity.
 */
@Repository
public interface ItemGroupRepository extends JpaRepository<ItemGroup, Long> {
    @Query(
        value = "select distinct itemGroup from ItemGroup itemGroup left join fetch itemGroup.users",
        countQuery = "select count(distinct itemGroup) from ItemGroup itemGroup"
    )
    Page<ItemGroup> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct itemGroup from ItemGroup itemGroup left join fetch itemGroup.users")
    List<ItemGroup> findAllWithEagerRelationships();

    @Query("select itemGroup from ItemGroup itemGroup left join fetch itemGroup.users where itemGroup.id =:id")
    Optional<ItemGroup> findOneWithEagerRelationships(@Param("id") Long id);
}
