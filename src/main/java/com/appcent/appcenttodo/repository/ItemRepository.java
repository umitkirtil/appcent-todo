package com.appcent.appcenttodo.repository;

import com.appcent.appcenttodo.domain.Item;
import com.appcent.appcenttodo.domain.ItemGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data SQL repository for the Item entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

    List<Item> findAllByItemGroup(ItemGroup itemGroup);

}
